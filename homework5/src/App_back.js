/*
import React, {useState, useEffect} from 'react';
import Store from 'store';

import './App.scss';

import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';

import Product from "./components/Product/Product";


export default function App() {

    const getCard = async () => {
        const _fileName='./data.json';

        const res = await fetch(`${_fileName}`)
        const body = await res.json();
        return body;
    }

    useEffect(()=> {

        if(productsArray.length===0) {
            getCard()
                .then((productsArray) => {
                    changeProductsArray(productsArray);

                    console.log(productsArray);
                });
        }

        console.log('cartArray', cartArray);

        if(cartArray.length>0) {
            Store.set('cart_items', cartArray);
        }

        if(favoriteArray.length>0) {
            Store.set('favorite_items', favoriteArray);
        }

    });


    const approveActionFavorite = () =>{

        changeModal1(false);


        changeFavoriteArray((oldFavoriteArray) => {
                const idx = oldFavoriteArray.indexOf(currentProduct);

                if(idx<0) {
                    return [...oldFavoriteArray, currentProduct];
                }else{

                    const favoriteArray = [
                        ...oldFavoriteArray.slice(0, idx),
                        ...oldFavoriteArray.slice(idx + 1)
                    ];

                    return favoriteArray;
                }
            })
    }


    const approveActionCart = () =>{

        changeModal2(false);

        changeCartArray((oldCartArray) => {
            const idx = oldCartArray.indexOf(currentProduct);

            if(idx<0) {
                return [...oldCartArray, currentProduct];
            }else{

                const cartArray = [
                    ...oldCartArray.slice(0, idx),
                    ...oldCartArray.slice(idx + 1)
                ];

                return  cartArray;
            }
        })

    }



    const openFirstModal = (id) =>{
        changeModal1(true);
        changeCurrentProduct(id);
    }

    const closeFirstModal= () =>{
        changeModal1(false);
    }

    const openSecondtModal = (id) =>{
        changeModal2(true);
        changeCurrentProduct(id);
    }

    const closeSecondModal= () =>{
        changeModal2(false);
    }

    const isFavoriteProduct = (id) => {
        const index=favoriteArray.indexOf(id);

        if(index<0)return false;

        return true;
    }

    const isInCartProduct = (id) => {
        const index=cartArray.indexOf(id);

        if(index<0)return false;

        return true;
    }


    const GetItemsArray = ()=>{


        return productsArray.map((product)=>{

            const isFavorite = isFavoriteProduct(product.id);
            const isInCart = isInCartProduct(product.id);

            const productActions = [
                (
                    <Button
                        key={"1"}
                        buttonClassName={"btn btn-primary"}
                        onClick={openFirstModal}
                        text={"Open Modal 1"}
                        backgroundColor={"pink"}
                        color={"blue"}
                        id={product.id}
                        type={"star"}
                        filled ={isFavorite}
                    />
                ),
                (
                    <Button
                        key={"2"}
                        buttonClassName={"btn btn-primary"}
                        onClick={openSecondtModal}
                        text={"Open Modal 2"}
                        backgroundColor={"gray"}
                        id={product.id}
                        type={"cart"}
                        filled ={isInCart}
                    />
                )
            ];

            return (
                <Product
                    key = {product.article}
                    product = {product}
                    productActions = {productActions}
                />
            );
        });
    }

    const GetModals =()=>{
        const actions = [
            (            <Button
                key={"3"}
                buttonClassName={"btn btn-primary"}
                onClick={approveActionFavorite}
                text={"Ok"}
                backgroundColor={"red"}
                color={"white"}
                type={"ok"}
            />),
            (            <Button
                key={"4"}
                buttonClassName={"btn btn-secondary"}
                onClick={closeFirstModal}
                text={"Cancel"}
                backgroundColor={"red"}
                color={"white"}
                type={"cancel"}
            />)
        ];

        const actions2 = [
            (            <Button
                key={"5"}
                buttonClassName={"btn btn-primary"}
                onClick={approveActionCart}
                text={"Ok"}
                backgroundColor={"red"}
                color={"white"}
                type={"ok"}
            />),
            (            <Button
                key={"6"}
                buttonClassName={"btn btn-secondary"}
                onClick={closeSecondModal}
                text={"Cancel"}
                backgroundColor={"red"}
                color={"white"}
                type={"cancel"}
            />)
        ];

        return (
            <React.Fragment>
                <Modal
                    key={1}
                    heder={"first"}
                    text={"Add to Favorite"}
                    actions={actions}
                    isOpen={modal1}
                    onClose={closeFirstModal}
                />

                <Modal
                    key={2}
                    heder={"Second"}
                    text={"Add to cart"}
                    actions={actions2}
                    isOpen={modal2}
                    onClose={closeSecondModal}
                />
            </React.Fragment>
    );

    }

      return (
        <div className="App">
            <GetItemsArray />
            <GetModals />
        </div>
    );

}
*/


// import Button from "./components/Button/Button";
// import Modal from "./components/Modal/Modal";
// import React from "react";
//
// <Button
//     key={"1"}
//     buttonClassName="btn btn-primary"
//     myOnClick={openModalFavorite}
//     text="Open Modal Favorite"
//     backgroundColor="pink"
//     color="blue"
//
// />
//
// <Button
// key={"2"}
// buttonClassName="btn btn-primary"
// myOnClick={openModalCart}
// text="Open Modal Cart"
// backgroundColor="pink"
// color="blue"
//
//     />
//
//     <Modal
// key={"3"}
// isOpen = {modalFavorite}
// onClose={closeModalFavorite}
// header ="Favorite"
// text="are you sure?"
// actions = {undefined}
// />
// <Modal
//     key={"4"}
//     isOpen = {modalCart}
//     onClose={closeModalCart}
//     header ="Cart"
//     text="are you sure?"
//     actions = {undefined}
//
// />