import Store from 'store';

const approveActionFavorite = (oldFavoriteArray, currentProduct) => {
    return (...oldFavoriteArray) => {
        const idx = oldFavoriteArray.indexOf(currentProduct);

        if (idx < 0) {
            Store.set('favorite_items', [...oldFavoriteArray, currentProduct]);
            return [...oldFavoriteArray, currentProduct];
        } else {
            const favoriteArray = [
                ...oldFavoriteArray.slice(0, idx),
                ...oldFavoriteArray.slice(idx + 1),
            ];

            Store.set('favorite_items', favoriteArray);

            return favoriteArray;
        }
    };
};

const approveActionCart = (oldCartArray, currentProduct) => {
    Store.set("cart_items", [...oldCartArray, currentProduct]);
    return (oldCartArray) => {
        const idx = oldCartArray.indexOf(currentProduct);

        if (idx < 0) {
            Store.set('cart_items', [...oldCartArray, currentProduct]);
            return [...oldCartArray, currentProduct];
        } else {

            const cartArray = [
                ...oldCartArray.slice(0, idx),
                ...oldCartArray.slice(idx + 1)
            ];
            Store.set('cart_items', cartArray);
            return cartArray;
        }
    }
};

const initialState = {
    modalFavorite: false,
    modalCart: false,
    currentProduct: undefined,
    productsArray: [],
    favoriteArray: [],
    cartArray: []
};
const productsReducer = (state = initialState, action) => {
    console.log('action type', action.type);
    switch (action.type) {
        case 'openModalFavorite':
            return {
                ...state,
                modalFavorite: true,
                currentProduct: action.id,
            };

        case 'closeModalFavorite':
            return {
                ...state,
                modalFavorite: false,
            };

        case 'openModalCart':
            return {
                ...state,
                modalCart: true,
                currentProduct: action.id,
            };

        case 'closeModalCart':
            return {
                ...state,
                modalCart: false,
            };

        case 'approveActionFavorite':
            return {
                ...state,
                favoriteArray: approveActionFavorite(
                    state.favoriteArray,
                    state.currentProduct
                ),
                modalFavorite: false
            };

        case 'approveActionCart':
            return {
                ...state,
                cartArray: approveActionCart(state.cartArray, state.currentProduct),
                modalFavorite: false
            };

        case 'changeProductsArray':
            return {
                ...state,
                productsArray: action.payload,
                modalFavorite: false
            };

        default:
            return  state;
    }
};

export default productsReducer;