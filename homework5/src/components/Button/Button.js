import React from 'react';
import './Button.scss';
import PropTypes from 'prop-types';

import {Star, Cart} from "../theme/icons";

function Button({id, myOnClick, text, backgroundColor, color, buttonClassName, image, type, filled}) {



    let innerBlock ='';

    switch(type){
        case "ok":
            innerBlock = text;
            break;

        case "cancel":
            innerBlock = text;
            break;

        case "star":
            innerBlock = (
                <Star
                    filled = {filled}
                />
            );
            break;

        case "cart":
            innerBlock = (
                <Cart
                    filled = {filled}
                />
            );
            break;

        default:
            innerBlock = (
                <button>
                    {text}
                </button>
            );
            break;
    }




    return (
        <div className="my-button"
             onClick = {()=>myOnClick()}
        >
            {innerBlock}
        </div>
    );

}

export default Button;


Button.propTypes = {
    text: PropTypes.string.isRequired,
    backgroundColor: PropTypes.string.isRequired,
    color: PropTypes.string,
    buttonClassName: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
};

Button.defaultProps = {
    color : 'pink'
};