import React from "react";
import ReduxForm from "../../forms/ReduxForm/ReduxForm";
import CartStyle from "./Cart.scss"

function Cart() {

    return(
        <div className="shoppingCart">
          <ReduxForm/>
        </div>
    )
}
export  default Cart;