import React from 'react';
import './Product.scss';

function Product ({product, productActions} ){

        const {name, price, url, article, color} = product;

        return (
            <div className={"product"}>
                <div>Name = {name}</div>
                <div>Article = {article}</div>
                <img className={"product-image"} src = {url} alt="product image"/>
                <div>Price = {price}</div>
                <div>Color = {color}</div>
                {productActions}
            </div>
        );
}

export default Product;