import Product from "./components/Product/Product";
import Modal from "./components/Modal/Modal";
import { bindActionCreators } from "redux";
import "./App.scss";
import Button from "./components/Button/Button";
import React, { useState, useEffect, Component } from "react";
import { connect } from "react-redux";
import {
    changeCartArray,
    changeFavoriteArray,
    closeModalFavorite,
} from "./store/actions/productsActions";
import Store from "store";
import "./App.scss";
import * as actions from "./store/actions/productsActions";
import AppRoutes from './AppRoutes';
import ReduxForm from "./forms/ReduxForm/ReduxForm";
import Cart from "./components/Cart/Cart";

class App extends Component {
    getCard = async () => {
        const _fileName = "./data.json";

        const res = await fetch(`${_fileName}`);
        const body = await res.json();
        return body;
    };

    getData = () => {
        const { changeProductsArray } = this.props;
        this.getCard().then((productsArray) => {
            changeProductsArray(productsArray);

            console.log(productsArray);
        });
    };

    componentDidMount() {
        this.getData();
    }

    render() {

        console.log(this.props);
        const {
            modalFavorite,
            modalCart,
            currentProduct,
            productsArray,
            favoriteArray,
            cartArray,
            closeModalFavorite,
            openModalFavorite,
            openModalCart,
            closeModalCart,
            changeProductArray,
            changeFavoriteArray,
            changeCartArray,
        } = this.props;

        const GetModals = ({
                               changeFavoriteArray,
                               closeModalFavorite,
                               changeCartArray,
                               closeModalCart,
                               modalFavorite,
                               modalCart,
                           }) => {
            const actions = [
                <Button
                    key={"3"}
                    buttonClassName={"btn btn-primary"}
                    myOnClick={changeFavoriteArray}
                    text={"Ok"}
                    backgroundColor={"red"}
                    color={"white"}
                    type={"ok"}
                />,
                <Button
                    key={"4"}
                    buttonClassName={"btn btn-secondary"}
                    myOnClick={closeModalFavorite}
                    text={"Cancel"}
                    backgroundColor={"red"}
                    color={"white"}
                    type={"cancel"}
                />,
            ];

            const actions2 = [
                <Button
                    key={"5"}
                    buttonClassName={"btn btn-primary"}
                    myOnClick={changeCartArray}
                    text={"Ok"}
                    backgroundColor={"red"}
                    color={"white"}
                    type={"ok"}
                />,
                <Button
                    key={"6"}
                    buttonClassName={"btn btn-secondary"}
                    myOnClick={closeModalCart}
                    text={"Cancel"}
                    backgroundColor={"red"}
                    color={"white"}
                    type={"cancel"}
                />,
            ];

            return (
                <React.Fragment>
                    <Modal
                        key={1}
                        heder={"first"}
                        text={"Add to Favorite"}
                        actions={actions}
                        isOpen={modalFavorite}
                        onClose={closeModalFavorite}
                    />

                    <Modal
                        key={2}
                        heder={"Second"}
                        text={"Add to cart"}
                        actions={actions2}
                        isOpen={modalCart}
                        onClose={closeModalCart}
                    />
                </React.Fragment>

            );
        };
        const GetItemsArray = () => {
            const isFavoriteProduct = (id, favoriteArray) => {
                const index = favoriteArray.indexOf(id);

                if (index < 0) return false;

                return true;
            };

            const isInCartProduct = (id, cartArray) => {
                const index = cartArray.indexOf(id);

                if (index < 0) return false;

                return true;
            };

            return productsArray.map((product) => {
                const isFavorite = isFavoriteProduct(product.id, favoriteArray);
                const isInCart = isInCartProduct(product.id, cartArray);

                const productActions = [
                    <Button
                        key={"1"}
                        buttonClassName={"btn btn-primary"}
                        myOnClick={openModalFavorite}
                        text={"Open Modal 1"}
                        backgroundColor={"pink"}
                        color={"blue"}
                        id={product.id}
                        type={"star"}
                        filled={isFavorite}
                    />,
                    <Button
                        key={"2"}
                        buttonClassName={"btn btn-primary"}
                        myOnClick={openModalCart}
                        text={"Open Modal 2"}
                        backgroundColor={"gray"}
                        id={product.id}
                        type={"cart"}
                        filled={isInCart}
                    />,
                ];

                return (
                    <Product
                        key={product.article}
                        product={product}
                        productActions={productActions}
                    />
                );
            });
        };


        return (
            <div className="App">
                <GetItemsArray
                    productsArray={productsArray}
                    favoriteArray={favoriteArray}
                    cartArray={cartArray}
                    openModalFavorite={openModalFavorite}
                    openModalCart={openModalCart}
                />
                <GetModals
                    changeFavoriteArray={changeFavoriteArray}
                    closeModalFavorite={closeModalFavorite}
                    changeCartArray={changeCartArray}
                    closeModalCart={closeModalCart}
                    modalFavorite={modalFavorite}
                    modalCart={modalCart}
                />
                <ReduxForm/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        modalFavorite: state.products.modalFavorite,
        modalCart: state.products.modalCart,
        currentProduct: state.products.currentProduct,
        productsArray: state.products.productsArray,
        favoriteArray: state.products.favoriteArray,
        cartArray: state.products.cartArray,
    };
};

export default connect(mapStateToProps, actions)(App);
