import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';
import Cart from './components/Cart/Cart';
import {reduxForm} from "redux-form";
import ReduxForm from "./forms/ReduxForm/ReduxForm";


class AppRoutes extends Component {
    render() {
        const {globalState} = this.props;
        const {currentDate} = globalState;

        return (
            <Switch>
                <Route exact path='/' component={Cart}/>
                <Route exact path='../../../store/forms/form' render={(props) => (
                    <Cart
                        date={currentDate} //для теста
                        {...props}
                    />
                )}/>
                <Route exact path='/body' render={() => (
                    <Cart

                    />
                )}/>
            </Switch>
        );
    }
}

export default AppRoutes;
